# Java TP10

## Premiere partie (fait en Td):

### Les constructeurs

##### ArbreVide
``` java
ArbreVide(){}
```

##### ArbreCons
``` java
ArbreCons(Integer val, Arbre Ag, Arbre Ad)
{
    this.racine = val; this.Ag = Ag; this.Ad = Ad;
}
ArbreCons(Integer val)
{
    this.racine = val; this.Ag = new ArbreVide(); this.Ad = new ArbreVide();
}
ArbreCons(int niveau)
{
    this.racine = null;
    if (niveau == 1)
    {
        this.Ag = new ArbreVide();
        this.Ad = new ArbreVide();
    }
    else
    {
         this.Ag = new ArbreCons(niveau - 1);
         this.Ad = new ArbreCons(niveau - 1);
    }
}
```
#### Creation des methodes abstraites
``` java
public abstract Integer getRacine();
public abstract Arbre getAg();
public abstract Arbre getAd();
public abstract void setRacine(Integer s);
public abstract void setAg(Arbre Ag);
public abstract void setAd(Arbre Ad);
public abstract boolean estVide();
public abstract void afficher();
public abstract Integer lePlusAGauche();
public abstract Arbre supprimer( Integer val );
public abstract boolean trouver( Integer val );
public abstract boolean estFeuille();
public abstract int nbFeuilles();
public abstract int nbNoeuds ();
public abstract int hauteur ();
public abstract Arbre insertTo(Integer val);
public abstract boolean recherche(Integer val);
public abstract void arbreEnTab(ArrayList L);
//public abstract boolean insereFeuille( Integer valeur);  //specif tournoi
//public abstract void placerGagnant( Integer s);  // specif tournoi
```
Les methodes abstraites sont initiées et l'objet arbre sera séparé en deux classes: ArbreVide et ArbreCons.

### getRacine
``` java
public abstract Integer getRacine();
```

###### Arbrevide:
``` java
public Integer getRacine() { return null; }
```
###### ArbreCons
``` java
public Integer getRacine()
{
    return this.racine;
}
```
La methode renvoie simplement la variable racine qui est initilialisée dans le constructeur.

### getAg
``` java
public abstract Arbre getAg();
```
renvoie l'arbre du coté gauche.
###### ArbreVide
``` java
public Integer getAg() { return null; }
```
###### ArbreCons
``` java
public Arbre getAg()
{
    return this.Ag;
}
```

### getAd
###### ArbreVide
``` java
public Integer getAd() { return null; }
```
###### ArbreCons
``` java
public Arbre getAd()
{
    return this.Ad;
}
```

### estVide
``` java
public abstract boolean estVide();
```
###### ArbreVide
``` java
public boolean estVide()
{
    return true;
}
```
###### ArbreCons
``` java
public boolean estVide()
{
    return false;
}
```

### lePlusAGauche
``` java
public abstract Integer lePlusAGauche();
```
###### ArbreVide
``` java
public Integer lePlusAGauche()
{
    return null;
}
```
###### ArbreCons
``` java
public Integer lePlusAGauche()
{
    this.getAg().lePlusAGauche();
    if (this.estFeuille())
    {
        return this.getRacine();
    }
    else
    {
        if(this.getAg() == null)
        {
            return this.getAd().lePlusAGauche();
        }
    return null;
    }
}
```
La fonction desend l'arbre en prennant l'arbre gauche a chaque fois. Puis si l'arbre gauche est vide ca verifie si c'est une feuille, si oui ca retourne la valeur sinon ca part a droite puis ca continue a la gauche.

### estFeuille
``` java
public abstract boolean estFeuille();
```
###### ArbreVide
``` java
public boolean estFeuille()
{
    return false;
}
```
###### ArbreCons
``` java
public boolean estFeuille()
{
    return this.getAg().estVide() && this.getAd().estVide();
}
```

### trouver
``` java
public abstract boolean trouver( Integer val );
```
###### ArbreVide
``` java
public boolean trouver (Integer val)
{
    return false;
}
```
###### ArbreCons
``` java
public boolean trouver (Integer val)
{
    return val.equals(this.getRacine()) || this.getAg().trouver(val) || this.getAd().trouver(val);
}
```

### insertTo
``` java
public abstract Arbre insertTo(Integer val);
```
###### ArbreVide
``` java
public Arbre insertTo(Integer val)
{
    return new ArbreCons(val);
}
```
###### ArbreCons
``` java
public Arbre insertTo(Integer val)
{
    if(!this.trouver(val))
    {
        if(val.compareTo(this.getRacine()) < 0)
        {
            return new ArbreCons(this.getRacine(),this.getAg().insertTo(val),this.getAd());
        }
        else
        {
            return new ArbreCons(this.getRacine(),this.getAg(),this.getAd().insertTo(val));
        }
    }
return this;
}
```

### recherche
``` java
public abstract boolean recherche(Integer val);
```
###### ArbreVide
``` java
public boolean recherche (Integer val)
{
    return false;
}
```
###### ArbreCons
``` java
public boolean recherche (Integer val)
{
    if(val.intValue()==this.getRacine().intValue())
    {
        return true;
    }
    if (val.compareTo(this.getRacine()) < 0)
    {
        return this.getAg().recherche(val);
    }
    else
    {
        return this.getAd().recherche(val);
    }
}
```