import java.util.ArrayList;

/**
 * Created by hugo on 11/24/2015.
 */


public abstract class ArbreBR {
    public abstract Integer getRacine();

    public abstract ArbreBR getAg();

    public abstract ArbreBR getAd();

    public abstract void setRacine(Integer s);

    public abstract void setAg(ArbreBR Ag);

    public abstract void setAd(ArbreBR Ad);

    public abstract boolean estVide();

    public abstract void afficher();

    public abstract Integer lePlusAGauche();

    public abstract ArbreBR supprimer(Integer val);

    public abstract boolean trouver(Integer val);

    public abstract boolean estFeuille();

    public abstract int nbFeuilles();

    public abstract int nbNoeuds();

    public abstract int hauteur();

    public abstract ArbreBR insertTo(Integer val);

    public abstract boolean recherche(Integer val);

    public abstract void arbreBREnTab(ArrayList<Integer> L);
    //public abstract boolean insereFeuille( Integer valeur);  //specif tournoi
    //public abstract void placerGagnant( Integer s);  // specif tournoi
}
    /**********ARBRE CONS***********/
    class ArbreBRCons extends ArbreBR
    {
        private Integer racine;
        private ArbreBR Ag;
        private ArbreBR Ad;
        public boolean estVide()
        {
            return false;
        }
        ArbreBRCons(Integer val, ArbreBR Ag, ArbreBR Ad)
        {
            this.racine = val; this.Ag = Ag; this.Ad = Ad;
        }
        ArbreBRCons(Integer val)
        {
            this.racine = val; this.Ag = new ArbreBRVide(); this.Ad = new ArbreBRVide();
        }
        ArbreBRCons(int niveau)
        {
            this.racine = null;
            if (niveau == 1)
            {
                this.Ag = new ArbreBRVide();
                this.Ad = new ArbreBRVide();
            }
            else
            {
                this.Ag = new ArbreBRCons(niveau - 1);
                this.Ad = new ArbreBRCons(niveau - 1);
            }
        }
        ArbreBRCons (ArrayList t, int start, int end)
        {
            if(start == end)
            {
                this.racine = (Integer)t.get(start);
            }
            else if(start == end - 1)
            {
                this.racine = (Integer) t.get(start);
                this.Ag = new ArbreBRVide();
                this.Ad = new ArbreBRCons((Integer)t.get(end));
            }
            else
            {
                int middle=(end + start)/2;
                this.racine =(Integer) t.get(middle);
                this.Ag = new ArbreBRCons(t, start, middle - 1);
                this.Ad = new ArbreBRCons(t, middle + 1, end);
            }
        }
        public Integer getRacine()
        {
            return this.racine;
        }
        public ArbreBR getAg()
        {
            return this.Ag;
        }
        public ArbreBR getAd()
        {
            return this.Ad;
        }
        public void setRacine(Integer s)
        {
            this.racine = s;
        }
        public void setAg(ArbreBR Ag)
        {
            this.Ag = Ag;
        }
        public void setAd(ArbreBR Ad)
        {
            this.Ad = Ad;
        }
        public boolean estFeuille()
        {
            return this.getAg().estVide() && this.getAd().estVide();
        }
        public void afficher()
        {
            System.out.print( this.getRacine()+" " );
            this.getAg().afficher();
            this.getAd().afficher();
        }
        public int nbNoeuds(){
            int nfg = 0, nfd =0;
            nfg = getAg().nbNoeuds();
            nfd = getAd().nbNoeuds();
            return (1 + nfg + nfd) ;
        }
        public int hauteur(){
            int nfg = 0, nfd = 0;
            nfg = 1 + getAg().hauteur();
            nfd = 1 + getAd().hauteur();
            return Math.max(nfg, nfd) ;
        }
        public Integer lePlusAGauche()
        {
            this.getAg().lePlusAGauche();
            if (this.estFeuille())
            {
                return this.getRacine();
            }
            else
            {
                if(this.getAg() == null)
                {
                    return this.getAd().lePlusAGauche();
                }
                return null;
            }
        }
        public boolean trouver (Integer val)
        {
            return val.equals(this.getRacine()) || this.getAg().trouver(val) || this.getAd().trouver(val);
        }
        public ArbreBR supprimer(Integer val){
            if(!this.getRacine().equals(val)){
                return new ArbreBRCons(this.getRacine(), this.getAg().supprimer(val),this.getAd().supprimer(val));
            }
            else{
                if(!this.getAd().estVide()){
                    return new ArbreBRCons(this.getAd().lePlusAGauche(),this.getAg(),this.getAd().supprimer(this.getAd().lePlusAGauche()));
                }
                else{
                    return this.getAg();
                }
            }
        }
        public int nbFeuilles()
        {
            if (this.estFeuille())
            {
                return 1;
            }
            else
            {
                return this.getAg().nbFeuilles() + this.getAd().nbFeuilles();
            }
        }
        public ArbreBR insertTo(Integer val)
        {
            if(!this.trouver(val))
            {
                if(val.compareTo(this.getRacine())<0)
                {
                    return new ArbreBRCons(this.getRacine(),this.getAg().insertTo(val),this.getAd());
                }
                else
                {
                    return new ArbreBRCons(this.getRacine(),this.getAg(),this.getAd().insertTo(val));
                }
            }
            return this;
        }
        public boolean recherche (Integer val)
        {
            if(val.intValue()==this.getRacine().intValue())
            {
                return true;
            }
            if (val.compareTo(this.getRacine()) < 0)
            {
                return this.getAg().recherche(val);
            }
            else
            {
                return this.getAd().recherche(val);
            }
        }
        public void arbreBREnTab (ArrayList <Integer> L)
        {
            this.getAg().arbreBREnTab(L);
            L.add(getRacine());
            this.getAd().arbreBREnTab(L);
        }
    }

    /**********ARBRE VIDE***********/
    class ArbreBRVide extends ArbreBR
    {
        ArbreBRVide ()
        {
        }
        public Integer getRacine() { return null; }
        public ArbreBR getAg() { return this; }
        public ArbreBR getAd() { return this; }
        public void setRacine(Integer s) {  }
        public void setAg(ArbreBR Ag) {  }
        public void setAd(ArbreBR Ad) {  }
        public boolean estVide()
        {
            return true;
        }
        public void afficher()
        {
            // System.out.print(" vide ");
        }
        public boolean estFeuille()
        {
            return false;
        }
        public int nbNoeuds ()
        {
            return 0;
        }
        public int hauteur ()
        {
            return 0;
        }
        public Integer lePlusAGauche()
        {
            return null;
        }
        public boolean trouver (Integer val)
        {
            return false;
        }
        public ArbreBR supprimer(Integer val)
        {
            return null;
        }
        public int nbFeuilles()
        {
            return 0;
        }
        public ArbreBR insertTo(Integer val)
        {
            return new ArbreBRCons(val);
        }
        public boolean recherche (Integer val)
        {
            return false;
        }
        public void arbreBREnTab(ArrayList <Integer> L) {}
    }
