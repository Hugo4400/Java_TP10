/**
 * Created by hugo on 11/25/2015.
 */
import java.util.ArrayList;

/**
 * Created by hugo on 11/24/2015.
 */


public abstract class ArbreBRFiche {
    public abstract Fiche getRacine();

    public abstract ArbreBRFiche getAg();

    public abstract ArbreBRFiche getAd();

    public abstract void setRacine(Fiche s);

    public abstract void setAg(ArbreBRFiche Ag);

    public abstract void setAd(ArbreBRFiche Ad);

    public abstract boolean estVide();

    public abstract void afficher();

    public abstract Fiche lePlusAGauche();

    public abstract ArbreBRFiche supprimer(Fiche val);

    public abstract boolean trouver(Fiche val);

    public abstract boolean estFeuille();

    public abstract int nbFeuilles();

    public abstract int nbNoeuds();

    public abstract int hauteur();

    public abstract ArbreBRFiche insertTo(Fiche val);

    public abstract boolean recherche(Fiche val);

    public abstract void arbreBREnTab(ArrayList<Fiche> L);
    //public abstract boolean insereFeuille( Fiche valeur);
}

    /**********ARBRE CONS***********/
    class ArbreBRFicheCons extends ArbreBRFiche
    {
        private Fiche racine;
        private ArbreBRFiche Ag;
        private ArbreBRFiche Ad;
        public boolean estVide()
        {
            return false;
        }
        ArbreBRFicheCons(Fiche val, ArbreBRFiche Ag, ArbreBRFiche Ad)
        {
            this.racine = val; this.Ag = Ag; this.Ad = Ad;
        }
        ArbreBRFicheCons(Fiche val)
        {
            this.racine = val; this.Ag = new ArbreBRFicheVide(); this.Ad = new ArbreBRFicheVide();
        }
        ArbreBRFicheCons(int niveau)
        {
            this.racine = null;
            if (niveau == 1)
            {
                this.Ag = new ArbreBRFicheVide();
                this.Ad = new ArbreBRFicheVide();
            }
            else
            {
                this.Ag = new ArbreBRFicheCons(niveau - 1);
                this.Ad = new ArbreBRFicheCons(niveau - 1);
            }
        }
        ArbreBRFicheCons (ArrayList t, int start, int end)
        {
            if(start == end)
            {
                this.racine = (Fiche)t.get(start);
            }
            else if(start == end - 1)
            {
                this.racine = (Fiche) t.get(start);
                this.Ag = new ArbreBRFicheVide();
                this.Ad = new ArbreBRFicheCons((Fiche)t.get(end));
            }
            else
            {
                int middle=(end + start)/2;
                this.racine =(Fiche) t.get(middle);
                this.Ag = new ArbreBRFicheCons(t, start, middle - 1);
                this.Ad = new ArbreBRFicheCons(t, middle + 1, end);
            }
        }
        public Fiche getRacine()
        {
            return this.racine;
        }
        public ArbreBRFiche getAg()
        {
            return this.Ag;
        }
        public ArbreBRFiche getAd()
        {
            return this.Ad;
        }
        public void setRacine(Fiche s)
        {
            this.racine = s;
        }
        public void setAg(ArbreBRFiche Ag)
        {
            this.Ag = Ag;
        }
        public void setAd(ArbreBRFiche Ad)
        {
            this.Ad = Ad;
        }
        public boolean estFeuille()
        {
            return this.getAg().estVide() && this.getAd().estVide();
        }
        public void afficher()
        {
            System.out.print( this.getRacine()+" " );
            this.getAg().afficher();
            this.getAd().afficher();
        }
        public int nbNoeuds(){
            int nfg = 0, nfd =0;
            nfg = getAg().nbNoeuds();
            nfd = getAd().nbNoeuds();
            return (1 + nfg + nfd) ;
        }
        public int hauteur(){
            int nfg = 0, nfd = 0;
            nfg = 1 + getAg().hauteur();
            nfd = 1 + getAd().hauteur();
            return Math.max(nfg, nfd) ;
        }
        public Fiche lePlusAGauche()
        {
            this.getAg().lePlusAGauche();
            if (this.estFeuille())
            {
                return this.getRacine();
            }
            else
            {
                if(this.getAg() == null)
                {
                    return this.getAd().lePlusAGauche();
                }
                return null;
            }
        }
        public boolean trouver (Fiche val)
        {
            return val.equals(this.getRacine()) || this.getAg().trouver(val) || this.getAd().trouver(val);
        }
        public ArbreBRFiche supprimer(Fiche val){
            if(!this.getRacine().equals(val)){
                return new ArbreBRFicheCons(this.getRacine(), this.getAg().supprimer(val),this.getAd().supprimer(val));
            }
            else{
                if(!this.getAd().estVide()){
                    return new ArbreBRFicheCons(this.getAd().lePlusAGauche(),this.getAg(),this.getAd().supprimer(this.getAd().lePlusAGauche()));
                }
                else{
                    return this.getAg();
                }
            }
        }
        public int nbFeuilles()
        {
            if (this.estFeuille())
            {
                return 1;
            }
            else
            {
                return this.getAg().nbFeuilles() + this.getAd().nbFeuilles();
            }
        }
        public ArbreBRFiche insertTo(Fiche val)
        {
            if(!this.trouver(val))
            {
                if(val.compareTo(this.getRacine())<0)
                {
                    return new ArbreBRFicheCons(this.getRacine(),this.getAg().insertTo(val),this.getAd());
                }
                else
                {
                    return new ArbreBRFicheCons(this.getRacine(),this.getAg(),this.getAd().insertTo(val));
                }
            }
            return this;
        }
        public boolean recherche (Fiche val)
        {
            if(val == this.getRacine())
            {
                return true;
            }
            if (val.compareTo(this.getRacine()) < 0)
            {
                return this.getAg().recherche(val);
            }
            else
            {
                return this.getAd().recherche(val);
            }
        }
        public void arbreBREnTab (ArrayList <Fiche> L)
        {
            this.getAg().arbreBREnTab(L);
            L.add(getRacine());
            this.getAd().arbreBREnTab(L);
        }
    }

    /**********ARBRE VIDE***********/
    class ArbreBRFicheVide extends ArbreBRFiche
    {
        ArbreBRFicheVide ()
        {
        }
        public Fiche getRacine() { return null; }
        public ArbreBRFiche getAg() { return this; }
        public ArbreBRFiche getAd() { return this; }
        public void setRacine(Fiche s) {  }
        public void setAg(ArbreBRFiche Ag) {  }
        public void setAd(ArbreBRFiche Ad) {  }
        public boolean estVide()
        {
            return true;
        }
        public void afficher()
        {
            // System.out.print(" vide ");
        }
        public boolean estFeuille()
        {
            return false;
        }
        public int nbNoeuds ()
        {
            return 0;
        }
        public int hauteur ()
        {
            return 0;
        }
        public Fiche lePlusAGauche()
        {
            return null;
        }
        public boolean trouver (Fiche val)
        {
            return false;
        }
        public ArbreBRFiche supprimer(Fiche val)
        {
            return null;
        }
        public int nbFeuilles()
        {
            return 0;
        }
        public ArbreBRFiche insertTo(Fiche val)
        {
            return new ArbreBRFicheCons(val);
        }
        public boolean recherche (Fiche val)
        {
            return false;
        }
        public void arbreBREnTab(ArrayList <Fiche> L) {}
    }


